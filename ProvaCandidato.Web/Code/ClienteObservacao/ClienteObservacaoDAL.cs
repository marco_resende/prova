﻿using ProvaCandidato.Data;
using ProvaCandidato.Data.Entidade;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProvaCandidato.Code
{
    public class ClienteObservacaoDAL : IDisposable
    {

        /// <summary>
        /// Consulta cliente por código
        /// </summary>
        /// <param name="Codigo">Código da cliente</param>
        /// <returns></returns>
        public List<ClienteObservacao> Consultar(int ClienteId)
        {
            return ctx.ClienteObservacoes.Where(f => f.ClienteId == ClienteId).ToList();
        }

        /// <summary>
        /// Cadastra cliente
        /// </summary>
        /// <param name="cliente">Cliente que será cadastrado</param>
        /// <returns></returns>
        public bool Inserir(ClienteObservacao observacao)
        {
            ctx.ClienteObservacoes.Add(observacao);
            bool sucesso = ctx.SaveChanges() > 0;
            return sucesso;
        }

        public void Dispose() { ctx.Dispose(); }

        #region Construtores e Atributos
        ContextoPrincipal ctx;
        public ClienteObservacaoDAL()
        {
            ctx = new ContextoPrincipal();
        }
        #endregion Construtores e Atributos
    }
}