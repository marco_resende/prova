﻿using ProvaCandidato.Code;
using ProvaCandidato.Data.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProvaCandidato.Controllers
{
    public class ClienteObservacaoController : SuperController
    {
        #region VIEWS
        public ActionResult _PartialCadastro(int ClienteId)
        {
            Cliente cliente = ClienteBLL.Consultar(ClienteId);
            if (cliente == null)
            {
                MensagemErro("Cliente não encontrado");
                return RedirectToAction("Cadastro", "Cliente", ClienteId);
            }
            ViewBag.Cliente = $"{cliente.Codigo} - {cliente.Nome}";
            return PartialView("_PartialCadastro", new ClienteObservacao(ClienteId));
        }

        #endregion VIEWS

        #region SERVIÇOS

        [HttpGet]
        public ActionResult Consultar(int ClienteId)
        {
            string msgFalha = $"Falha ao consultar observações do cliente.";
            try
            {
                string validacao = "";
                var resultados = ClienteObservacaoBLL.Consultar(ClienteId, out validacao);
                return RetornoJson(resultados, msgFalha, validacao: validacao);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        [HttpPost]
        public ActionResult Inserir(ClienteObservacao observacao)
        {
            string msgSucesso = $"Observação cadastrada com sucesso.";
            string msgFalha = $"Falha ao cadastrar observação.";
            try
            {
                if (!ModelState.IsValid)
                    return JsonErro(msgFalha, "Modelo inválido!");

                string validacao = "";
                bool sucesso = ClienteObservacaoBLL.Inserir(observacao, out validacao);
                return RetornoJson(sucesso, msgFalha, msgSucesso, validacao);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        #endregion SERVIÇOS
    }
}