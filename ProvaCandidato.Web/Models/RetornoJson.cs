﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProvaCandidato.Models
{
    public class RetornoJson<T>
    {
        public bool Sucesso { get; set; }
        public T Dados{ get; set; }
        public Alerta Alerta { get; set; }

        #region CONSTRUTORES
        public RetornoJson(){}
        public RetornoJson(bool sucesso, T dados, string mensagem = "", TipoAlertaEnum tipo = TipoAlertaEnum.Sucesso, string erro = "")
        {
            this.Sucesso = sucesso;
            string obj = JsonConvert.SerializeObject(dados);
            this.Dados = JsonConvert.DeserializeObject<T>(obj);
            if (!mensagem.IsEmpty() || !erro.IsEmpty())
                this.Alerta = new Alerta(tipo, $"{mensagem.Trim()} {erro.Trim()}".Trim());
        }
        #endregion CONSTRUTORES
    }
}