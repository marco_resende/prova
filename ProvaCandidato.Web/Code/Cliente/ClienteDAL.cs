﻿using ProvaCandidato.Data;
using ProvaCandidato.Data.Entidade;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProvaCandidato.Code
{
    public class ClienteDAL : IDisposable
    {
        /// <summary>
        /// Consulta lista de clientes
        /// </summary>
        /// <returns></returns>
        public List<Cliente> ConsultarLista()
        {
            return ctx.Clientes
                .Include(f => f.Cidade)
                .Include(f => f.ClienteObservacao)
                .ToList();
        }

        /// <summary>
        /// Consulta cliente por código
        /// </summary>
        /// <param name="Codigo">Código da cliente</param>
        /// <returns></returns>
        public Cliente Consultar(int Codigo)
        {
            return ctx.Clientes
                .Include(f => f.Cidade)
                .Include(f => f.ClienteObservacao)
                .FirstOrDefault(f => f.Codigo == Codigo);
        }

        /// <summary>
        /// Cadastra cliente
        /// </summary>
        /// <param name="cliente">Cliente que será cadastrado</param>
        /// <returns></returns>
        public bool Inserir(Cliente cliente)
        {
            ctx.Clientes.Add(cliente);
            bool sucesso = ctx.SaveChanges() > 0;
            return sucesso;
        }

        /// <summary>
        /// Atualiza cliente
        /// </summary>
        /// <param name="cliente">Cliente que será atualizado</param>
        /// <returns></returns>
        public bool Atualizar(Cliente cliente)
        {
            ctx.Entry(cliente).State = EntityState.Modified;
            bool sucesso = ctx.SaveChanges() > 0;
            return sucesso;
        }

        /// <summary>
        /// Deleta cliente
        /// </summary>
        /// <param name="Codigo">Codigo da cliente que será deletado</param>
        /// <returns></returns>
        public bool Deletar(int Codigo)
        {
            Cliente cliente = ctx.Clientes.Find(Codigo);
            ctx.Clientes.Remove(cliente);
            return ctx.SaveChanges() > 0;
        }

        public void Dispose() { ctx.Dispose(); }

        #region Construtores e Atributos
        ContextoPrincipal ctx;
        public ClienteDAL()
        {
            ctx = new ContextoPrincipal();
        }
        #endregion Construtores e Atributos
    }
}