﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProvaCandidato.Data.Entidade
{
    [Table("ClienteObservacao")]
    public class ClienteObservacao
    {
        [Key]
        [Column("codigo")]
        [Display(Name = "Código")]
        public int Codigo { get; set; }

        [StringLength(500)]
        [MinLength(3)]
        [MaxLength(500)]
        [Required]
        [Column("observacao")]
        [Display(Name = "Observação")]
        public string Observacao { get; set; }

        [Column("codigo_cliente")]
        [Display(Name = "Cliente")]
        [ForeignKey("Cliente")]
        public int ClienteId { get; set; }

        [Display(Name = "Cliente")]
        [JsonIgnore]
        public virtual Cliente Cliente { get; set; }

        #region CONSTRUTORES
        public ClienteObservacao(){}
        public ClienteObservacao(int ClienteId)
        {
            this.ClienteId = ClienteId;
        }
        #endregion CONSTRUTORES

    }
}