﻿using System.Web;
using System.Web.Optimization;

namespace ProvaCandidato
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Assets/Scripts/jquery-{version}.js",
                        "~/Assets/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Assets/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Assets/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Assets/Scripts/sweetalert2@10.js",
                      "~/Assets/Scripts/bootstrap.js",
                      "~/Assets/Scripts/moment.min.js",
                      "~/Assets/Scripts/jquery.dataTables.min.js",
                      "~/Assets/Scripts/dataTables.buttons.min.js",
                      "~/Assets/Scripts/geral.js",
                      "~/Assets/Scripts/select2.js",
                      "~/Assets/Scripts/HttpAjax.js",
                      "~/Assets/FontAwesome/fontawesome.min.js",
                      "~/Assets/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Assets/css").Include(
                      "~/Assets/StyleSheets/bootstrap_4.css",
                      "~/Assets/StyleSheets/bootstrap.css",
                      "~/Assets/StyleSheets/sweetalert2.min.css",
                      "~/Assets/StyleSheets/small-buttons.css",
                      "~/Assets/StyleSheets/jquery.dataTables.min.css",
                      "~/Assets/StyleSheets/loading.css",
                      "~/Assets/StyleSheets/geral.css",
                      "~/Assets/StyleSheets/select2.css",
                      "~/Assets/FontAwesome/fontawesome.min.css",
                      "~/Assets/themes/base/jquery*"));

           
        }
    }
}
