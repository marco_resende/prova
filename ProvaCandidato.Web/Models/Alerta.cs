﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProvaCandidato.Models
{
    public class Alerta
    {
        public TipoAlertaEnum Tipo { get; set; }
        public string Message { get; set; }
        public string CssClassName
        {
            get
            {
                switch (Tipo)
                {
                    case TipoAlertaEnum.Sucesso:
                        return "alert-success";
                    case TipoAlertaEnum.Atencao:
                        return "alert-warning";
                    case TipoAlertaEnum.Erro:
                        return "alert-danger";
                    default:
                        return "alert-info";
                }
            }
        }
        public string Title
        {
            get
            {
                switch (Tipo)
                {
                    case TipoAlertaEnum.Sucesso:
                        return "Sucesso";
                    case TipoAlertaEnum.Atencao:
                        return "Atenção";
                    case TipoAlertaEnum.Erro:
                        return "Erro";
                    case TipoAlertaEnum.Informativo:
                        return "Informativo";
                    default:
                        return "Alerta";
                }
            }
        }

        #region CONSTRUTORES
        public Alerta() { }

        public Alerta(TipoAlertaEnum tipo, string message)
        {
            this.Tipo = tipo;
            this.Message = message;
        }
        #endregion CONSTRUTORES
    }
}