﻿using ProvaCandidato.Data;
using ProvaCandidato.Data.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProvaCandidato.Code
{
    public static class ClienteObservacaoBLL
    {
        /// <summary>
        /// Consulta lista de clientes
        /// </summary>
        /// <returns></returns>
        public static List<ClienteObservacao> Consultar(int ClienteId, out string validacao)
        {
            try
            {
                validacao = "";
                if (ClienteId == 0)
                {
                    validacao = "Cliente não informado para consulta.";
                    return null;
                }

                using (var DAL = new ClienteObservacaoDAL())
                    return DAL.Consultar(ClienteId);
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Cadastra cliente
        /// </summary>
        /// <param name="observacao">Observacao que será cadastrada</param>
        /// <param name="validacao">Validacação do objeto - Vazio se objeto for valido</param>
        /// <returns></returns>
        public static bool Inserir(ClienteObservacao observacao, out string validacao)
        {
            try
            {
                validacao = ValidarObjeto(ref observacao);
                if (!string.IsNullOrEmpty(validacao))
                    return false;

                using (var DAL = new ClienteObservacaoDAL())
                    return DAL.Inserir(observacao);
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        private static string ValidarObjeto(ref ClienteObservacao observacao)
        {
            if (observacao == null || observacao.Observacao.IsEmpty())
                return "Observação não informada.";
            else
                observacao.Observacao = observacao.Observacao.Trim();
            
            return "";
        }
    }
}