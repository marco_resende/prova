﻿using ProvaCandidato.Data;
using ProvaCandidato.Data.Entidade;
using ProvaCandidato.Helper;
using ProvaCandidato.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProvaCandidato.Controllers
{
    public class SuperController : Controller
    {
        protected internal JsonResult RetornoJson<T>(T dados, string msgFalha, string msgSucesso = "", string validacao = "", string emptyMsg = "Nenhum registro encontrado.", bool tempMessage = true)
        {
            try
            {
                MessageHelper.ClearDisplayMessage(this);
                if (!validacao.IsEmpty())
                    msgFalha += $" {validacao}";

                if (typeof(T) == typeof(bool))
                    return ProcessaBool(Convert.ToBoolean(dados), msgFalha, msgSucesso);
                else
                    return ProcessaObjeto(dados, msgFalha, msgSucesso, emptyMsg);
            }
            catch (Exception ex)
            {
                return JsonR(new RetornoJson<T>(false, dados, msgFalha, TipoAlertaEnum.Erro, ex.Message));
            }
            finally
            {
                if (!tempMessage)
                    MessageHelper.ClearDisplayMessage(this);
            }
        }

        private JsonResult ProcessaBool(bool dado, string msgFalha, string msgSucesso)
        {
            RetornoJson<bool> retorno = dado ?
                new RetornoJson<bool>(dado, dado, msgSucesso) :
                new RetornoJson<bool>(dado, dado, msgFalha, TipoAlertaEnum.Erro);
            return JsonR(retorno);
        }

        private JsonResult ProcessaObjeto<T>(T dados, string msgFalha, string msgSucesso, string emptyMsg)
        {
            var retorno = dados == null ?
                new RetornoJson<T>(true, dados, string.IsNullOrEmpty(emptyMsg) ? emptyMsg : msgFalha, TipoAlertaEnum.Atencao) :
                new RetornoJson<T>(true, dados, msgSucesso);
            return JsonR(retorno);
        }

        protected internal JsonResult JsonR<T>(RetornoJson<T> obj)
        {
            MessageHelper.DisplayMessage(this, obj.Alerta);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        protected internal JsonResult JsonErro(string msgErro, string msgExcecao = null)
        {
            return JsonR(new RetornoJson<object>(false, null, msgErro, TipoAlertaEnum.Erro, msgExcecao));
        }

        protected internal void MensagemErro(string mensagem)
        {
            Alerta alerta = new Alerta(TipoAlertaEnum.Erro, mensagem);
            MessageHelper.DisplayMessage(this, alerta);
        }
    }
}