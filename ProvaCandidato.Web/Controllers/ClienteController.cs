﻿using ProvaCandidato.Code;
using ProvaCandidato.Data.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProvaCandidato.Controllers
{
    public class ClienteController : SuperController
    {
        #region VIEWS
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Cadastro(int? Codigo = null)
        {
            try
            {
                Cliente cliente = null;
                if (Codigo.HasValue)
                {
                    cliente = ClienteBLL.Consultar(Codigo.Value);
                    if (cliente == null)
                    {
                        MensagemErro("Cliente não encontrada!");
                        return RedirectToAction("Index", "Cliente");
                    }
                }
                else
                    cliente = new Cliente();

                return View(cliente);
            }
            catch (Exception ex)
            {
                MensagemErro(ex.Message);
                return RedirectToAction("Index", "Cliente");
            }
        }
        #endregion VIEWS

        #region SERVIÇOS

        [HttpGet]
        public ActionResult Consultar(int? Codigo = null)
        {
            string msgFalha = $"Falha ao consultar cliente{(Codigo.HasValue ? "s" : "")}";
            try
            {
                if (Codigo.HasValue)
                    return RetornoJson(ClienteBLL.Consultar(Codigo.Value), msgFalha);
                else
                    return RetornoJson(ClienteBLL.ConsultarLista(), msgFalha);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        [HttpPost]
        public ActionResult Inserir(Cliente cliente)
        {
            string msgSucesso = $"Cliente cadastrado com sucesso.";
            string msgFalha = $"Falha ao cadastrar cliente.";
            try
            {
                if (!ModelState.IsValid)
                    return JsonErro(msgFalha, "Modelo inválido!");

                string validacao = "";
                bool sucesso = ClienteBLL.Inserir(cliente, out validacao);
                return RetornoJson(sucesso, msgFalha, msgSucesso, validacao);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        [HttpPut]
        public ActionResult Atualizar(Cliente cliente)
        {
            string msgSucesso = $"Cliente atualizado com sucesso!";
            string msgFalha = $"Falha ao atualizar cliente.";
            try
            {
                if (!ModelState.IsValid)
                    return JsonErro(msgFalha, "Modelo inválido!");

                string validacao = "";
                bool sucesso = ClienteBLL.Atualizar(cliente, out validacao);
                return RetornoJson(sucesso, msgFalha, msgSucesso, validacao);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        [HttpDelete]
        public ActionResult Deletar(Cliente cliente)
        {
            string msgSucesso = $"Cliente deletado com sucesso!";
            string msgFalha = $"Falha ao deletar cliente.";
            try
            {
                string validacao = "";
                bool sucesso = ClienteBLL.Deletar(cliente, out validacao);
                return RetornoJson(sucesso, msgFalha, msgSucesso, validacao, tempMessage: false);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        #endregion SERVIÇOS
    }
}