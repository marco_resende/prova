﻿using ProvaCandidato.Data;
using ProvaCandidato.Data.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProvaCandidato.Code
{
    public class CidadeDAL : IDisposable
    {
        /// <summary>
        /// Consulta lista de cidades
        /// </summary>
        /// <returns></returns>
        public List<Cidade> ConsultarLista()
        {
            return ctx.Cidades.ToList();
        }

        /// <summary>
        /// Consulta cidade por código
        /// </summary>
        /// <param name="Codigo">Código da cidade</param>
        /// <returns></returns>
        public Cidade Consultar(int Codigo)
        {
            return ctx.Cidades.Find(Codigo);
        }

        /// <summary>
        /// Cadastra cidade
        /// </summary>
        /// <param name="cidade">Cidade que será cadastrada</param>
        /// <returns></returns>
        public bool Inserir(Cidade cidade)
        {
            ctx.Cidades.Add(cidade);
            bool sucesso = ctx.SaveChanges() > 0;
            return sucesso;
        }

        /// <summary>
        /// Cadastra cidade
        /// </summary>
        /// <param name="cidade">Cidade que será cadastrada</param>
        /// <returns></returns>
        public bool Atualizar(Cidade cidade)
        {
            ctx.Entry(cidade).State = System.Data.Entity.EntityState.Modified;
            bool sucesso = ctx.SaveChanges() > 0;
            return sucesso;
        }

        /// <summary>
        /// Deleta cidade
        /// </summary>
        /// <param name="Codigo">Codigo da cidade que será deletada</param>
        /// <returns></returns>
        public bool Deletar(int Codigo)
        {
            Cidade cidade = ctx.Cidades.Find(Codigo);
            ctx.Cidades.Remove(cidade);
            return ctx.SaveChanges() > 0;
        }

        public void Dispose() { ctx.Dispose(); }

        #region Construtores e Atributos
        ContextoPrincipal ctx;
        public CidadeDAL()
        {
            ctx = new ContextoPrincipal();
        }
        #endregion Construtores e Atributos
    }
}