﻿function Redirect(url) {
    window.location.assign(url);
}

Object.defineProperty(Object.prototype, "serializeObject", {
    value: function serializeObject() {
        var obj = {};
        for (var i = 0; i < this.length; i++) {
            obj[this[i]['name']] = this[i]['value'];
        }
        return obj;
    },
    writable: true,
    configurable: true
});

function gerarDropDown(lista, VALOR, TEXTO, DROPDOWN, defaultValue = null, removeLoading = false) {
    var html = '';
    if (lista != null) {
        lista.forEach((item) => {
            html += `<option value="${item[VALOR]}">${item[TEXTO]}</option>`;
        });
    }
    var primeiroValor = `<option selected value=''>Selecione...</option>`;
    $(DROPDOWN).html(primeiroValor + html);
    defaultValue = defaultValue == null ? '' : defaultValue;
    $(DROPDOWN).val(defaultValue).trigger("change");
    if (removeLoading)
        jsLoading(false);
}

