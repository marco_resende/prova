﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProvaCandidato.Data.Entidade
{
    [Table("Cliente")]
    public class Cliente
    {
        [Key]
        [Column("codigo")]
        [Display(Name = "Código")]
        public int Codigo { get; set; }

        [StringLength(50)]
        [MinLength(3)]
        [MaxLength(50)]
        [Required]
        [Column("nome")]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Column("data_nascimento")]
        [Display(Name = "Data de Nascimento")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? DataNascimento { get; set; }

        [Column("codigo_cidade")]
        [Display(Name = "Cidade")]
        [ForeignKey("Cidade")]
        public int CidadeId { get; set; }

        [Display(Name = "Ativo")]
        public bool Ativo { get; set; }

        [Display(Name = "Cidade")]
        public virtual Cidade Cidade { get; set; }

        [Display(Name = "Observação")]
        [InverseProperty("Cliente")]
        public virtual ICollection<ClienteObservacao> ClienteObservacao { get; set; }

    }
}