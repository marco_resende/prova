﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class StringExtension
{
    public static bool IsEmpty(this string value)
    {
        bool empty = string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value);
        if (empty)
            value = "";
        return empty;
    }
}