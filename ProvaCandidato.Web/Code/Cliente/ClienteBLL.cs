﻿using ProvaCandidato.Data;
using ProvaCandidato.Data.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProvaCandidato.Code
{
    public static class ClienteBLL
    {
        /// <summary>
        /// Consulta lista de clientes
        /// </summary>
        /// <returns></returns>
        public static List<Cliente> ConsultarLista()
        {
            try
            {
                using (var DAL = new ClienteDAL())
                    return DAL.ConsultarLista();
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Consulta cliente por código
        /// </summary>
        /// <param name="Codigo">Código da cliente</param>
        /// <returns></returns>
        public static Cliente Consultar(int Codigo)
        {
            try
            {
                using (var DAL = new ClienteDAL())
                    return DAL.Consultar(Codigo);
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Cadastra cliente
        /// </summary>
        /// <param name="cliente">Cliente que será cadastrado</param>
        /// <param name="validacao">Validacação do objeto - Vazio se objeto for valido</param>
        /// <returns></returns>
        public static bool Inserir(Cliente cliente, out string validacao)
        {
            try
            {
                validacao = ValidarCliente(ref cliente);
                if (!string.IsNullOrEmpty(validacao))
                    return false;

                using (var DAL = new ClienteDAL())
                    return DAL.Inserir(cliente);
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Atualiza cliente
        /// </summary>
        /// <param name="cliente">Cliente que será atualizado</param>
        /// <param name="validacao">Validacação do objeto - Vazio se objeto for valido</param>
        /// <returns></returns>
        public static bool Atualizar(Cliente cliente, out string validacao)
        {
            try
            {
                validacao = ValidarCliente(ref cliente);
                if (!string.IsNullOrEmpty(validacao))
                    return false;

                using (var DAL = new ClienteDAL())
                    return DAL.Atualizar(cliente);
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Deleta cliente
        /// </summary>
        /// <param name="cliente">Cliente que será deletado</param>
        /// <param name="erro">Mensagem de erro</param>
        /// <returns></returns>
        public static bool Deletar(Cliente cliente, out string erro)
        {
            erro = "";
            try
            {
                bool sucesso = false;
                if (cliente == null || cliente.Codigo == 0)
                    erro = "Cliente não informada.";
                else
                {
                    using (var DAL = new ClienteDAL())
                        sucesso = DAL.Deletar(cliente.Codigo);
                }

                return sucesso;
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        private static string ValidarCliente(ref Cliente cliente)
        {
            if (cliente == null)
                return "Cliente não informado.";
            else if (cliente.Nome.IsEmpty())
                return "Nome não informado.";
            else
            {
                cliente.Nome = cliente.Nome.Trim();
                if (cliente.Nome.Length < 3)
                    return "Nome deve ter no mínimo 3 caracteres.";
                else if (cliente.Nome.Length > 50)
                    return "Nome deve ter no máximo 50 caracteres.";
            }

            if (cliente.DataNascimento > DateTime.Now)
                return "A data de nascimento do cliente não pode ser posterior ao dia de hoje.";

            return "";
        }
    }
}