﻿using System;
using System.Diagnostics;

public static class ExceptionExtension
{
    public static Exception Handle(this Exception ex, string parametro = "")
    {
        var stackTrace = new StackTrace();
        var classe = stackTrace.GetFrame(1).GetMethod().ReflectedType.Name;
        var metodo = stackTrace.GetFrame(1).GetMethod().Name;
        if (!string.IsNullOrEmpty(parametro))
            parametro = $" (Parametro: {parametro})";
        return new Exception(string.Format("Falha na execução [{0} - {1}{2}]. Mensagem: {3}", classe, metodo, parametro, ex.Message));
    }
}
