﻿function Get(url, fnCallBackSucesso, fnCallBackErro = undefined, controlaLoading = true, atributo = "") {
    jsLoading(true, controlaLoading);
    $.get(url, (response) => { ProcessaRequest(response, controlaLoading, atributo, fnCallBackSucesso, fnCallBackErro); });
}

function Post(url, data, fnCallBackSucesso, fnCallBackErro = undefined, controlaLoading = true, atributo = "") {
    jsLoading(true, controlaLoading);
    $.post(url, data, (response) => { ProcessaRequest(response, controlaLoading, atributo, fnCallBackSucesso, fnCallBackErro); });
}

function Patch(url, data, fnCallBackSucesso, fnCallBackErro = undefined, controlaLoading = true, atributo = "") {
    AjaxRequest(url, data, fnCallBackSucesso, fnCallBackErro, controlaLoading, atributo, "PATCH");
}

function Put(url, data, fnCallBackSucesso, fnCallBackErro = undefined, controlaLoading = true, atributo = "") {
    AjaxRequest(url, data, fnCallBackSucesso, fnCallBackErro, controlaLoading, atributo, "PUT");
}

function Delete(url, data, fnCallBackSucesso, fnCallBackErro = undefined, controlaLoading = true, atributo = "") {
    AjaxRequest(url, data, fnCallBackSucesso, fnCallBackErro, controlaLoading, atributo, "DELETE");
}

function AjaxRequest(url, data, fnCallBackSucesso, fnCallBackErro, controlaLoading, atributo, type) {
    $.ajax({
        url: url,
        data: data,
        type: type,
        success: (response) => {
            ProcessaRequest(response, controlaLoading, atributo, fnCallBackSucesso, fnCallBackErro);
        }
    });
}

function ProcessaRequest(response, controlaLoading, atributo, fnCallBackSucesso, fnCallBackErro) {
    console.log(response);
    jsLoading(false, controlaLoading);
    var dados = atributo == "" ? response : response[atributo];
    if (response.Sucesso) {
        if (fnCallBackSucesso != undefined && fnCallBackSucesso != null)
            fnCallBackSucesso(dados);
    }
    else {
        if (fnCallBackErro != undefined && fnCallBackErro != null)
            fnCallBackErro(response);
    }

    if (response.Alerta != null)
        showAlertObj(response.Alerta);
}