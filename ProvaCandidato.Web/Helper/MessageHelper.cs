﻿using ProvaCandidato.Models;
using System;
using System.Web.Mvc;

namespace ProvaCandidato.Helper
{
    public static class MessageHelper
    {
        public static void ClearDisplayMessage(Controller controller)
        {
            controller.TempData["UserMessage"] = null;
        }

        public static void DisplayMessage(Controller controller, Alerta alerta)
        {
            controller.TempData["UserMessage"] = alerta;
        }

        public static void DisplayMessage(Controller controller, string message, TipoAlertaEnum tipo)
        {
            if (!string.IsNullOrEmpty(message))
            {
                var alerta = new Alerta(tipo, message);
                DisplayMessage(controller, alerta);
            }
        }
    }
}