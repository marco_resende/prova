﻿using ProvaCandidato.Code;
using ProvaCandidato.Data.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProvaCandidato.Controllers
{
    public class CidadeController : SuperController
    {
        #region VIEWS
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Cadastro(int? Codigo = null)
        {
            try
            {
                Cidade cidade = null;
                if (Codigo.HasValue)
                {
                    cidade = CidadeBLL.Consultar(Codigo.Value);
                    if (cidade == null)
                    {
                        MensagemErro("Cidade não encontrada!");
                        return RedirectToAction("Index", "Cidade");
                    }
                }
                else
                    cidade = new Cidade();

                return View(cidade);
            }
            catch (Exception ex)
            {
                MensagemErro(ex.Message);
                return RedirectToAction("Index", "Cliente");
            }
        }
        #endregion VIEWS

        #region SERVIÇOS
        [HttpGet]
        public ActionResult Consultar(int? Codigo = null)
        {
            string msgFalha = $"Falha ao consultar cidade{(Codigo.HasValue ? "s" : "")}";
            try
            {
                if (Codigo.HasValue)
                    return RetornoJson(CidadeBLL.Consultar(Codigo.Value), msgFalha);
                else
                    return RetornoJson(CidadeBLL.ConsultarLista(), msgFalha);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        [HttpPost]
        public ActionResult Inserir(Cidade cidade)
        {
            string msgSucesso = $"Cidade cadastrada com sucesso.";
            string msgFalha = $"Falha ao cadastrar cidade.";
            try
            {
                if (!ModelState.IsValid)
                    return JsonErro(msgFalha, "Modelo inválido!");

                string validacao = "";
                bool sucesso = CidadeBLL.Inserir(cidade, out validacao);
                return RetornoJson(sucesso, msgFalha, msgSucesso, validacao);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        [HttpPut]
        public ActionResult Atualizar(Cidade cidade)
        {
            string msgSucesso = $"Cidade atualizada com sucesso!";
            string msgFalha = $"Falha ao atualizar cidade.";
            try
            {
                if (!ModelState.IsValid)
                    return JsonErro(msgFalha, "Modelo inválido!");

                string validacao = "";
                bool sucesso = CidadeBLL.Atualizar(cidade, out validacao);
                return RetornoJson(sucesso, msgFalha, msgSucesso, validacao);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        [HttpDelete]
        public ActionResult Deletar(Cidade cidade)
        {
            string msgSucesso = $"Cidade deletada com sucesso!";
            string msgFalha = $"Falha ao deletar cidade.";
            try
            {
                string validacao = "";
                bool sucesso = CidadeBLL.Deletar(cidade, out validacao);
                return RetornoJson(sucesso, msgFalha, msgSucesso, validacao, tempMessage: false);
            }
            catch (Exception ex)
            {
                return JsonErro(msgFalha, ex.Message);
            }
        }

        #endregion SERVIÇOS
    }
}