﻿using ProvaCandidato.Data;
using ProvaCandidato.Data.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProvaCandidato.Code
{
    public static class CidadeBLL
    {
        /// <summary>
        /// Consulta lista de cidades
        /// </summary>
        /// <returns></returns>
        public static List<Cidade> ConsultarLista()
        {
            try
            {
                using(var DAL = new CidadeDAL())
                    return DAL.ConsultarLista();
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Consulta cidade por código
        /// </summary>
        /// <param name="Codigo">Código da cidade</param>
        /// <returns></returns>
        public static Cidade Consultar(int Codigo)
        {
            try
            {
                using (var DAL = new CidadeDAL())
                    return DAL.Consultar(Codigo);
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Cadastra cidade
        /// </summary>
        /// <param name="cidade">Cidade que será cadastrada</param>
        /// <param name="validacao">Validacação do objeto - Vazio se objeto for valido</param>
        /// <returns></returns>
        public static bool Inserir(Cidade cidade, out string validacao)
        {
            try
            {
                validacao = ValidarCidade(ref cidade);
                if (!string.IsNullOrEmpty(validacao))
                    return false;

                using (var DAL = new CidadeDAL())
                    return DAL.Inserir(cidade);
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Atualiza cidade
        /// </summary>
        /// <param name="cidade">Cidade que será atualizada</param>
        /// <param name="validacao">Validacação do objeto - Vazio se objeto for valido</param>
        /// <returns></returns>
        public static bool Atualizar(Cidade cidade, out string validacao)
        {
            try
            {
                validacao = ValidarCidade(ref cidade);
                if (!string.IsNullOrEmpty(validacao))
                    return false;

                using (var DAL = new CidadeDAL())
                    return DAL.Atualizar(cidade);
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        /// <summary>
        /// Deleta cidade
        /// </summary>
        /// <param name="cidade">Cidade que será deletada</param>
        /// <param name="erro">Mensagem de erro</param>
        /// <returns></returns>
        public static bool Deletar(Cidade cidade, out string erro)
        {
            erro = "";
            try
            {
                bool sucesso = false;
                if (cidade == null || cidade.Codigo == 0)
                    erro = "Cidade não informada.";
                else
                {
                    using (var DAL = new CidadeDAL())
                        sucesso =  DAL.Deletar(cidade.Codigo);
                }

                return sucesso;
            }
            catch (Exception ex)
            {
                throw ex.Handle();
            }
        }

        private static string ValidarCidade(ref Cidade cidade)
        {
            if (cidade.Nome.IsEmpty())
                return "Nome não informado.";
            else
            {
                cidade.Nome = cidade.Nome.Trim();
                if (cidade.Nome.Length < 3)
                    return "Nome deve ter no mínimo 3 caracteres.";
                else if (cidade.Nome.Length > 50)
                    return "Nome deve ter no máximo 50 caracteres.";
            }

            return "";
        }
    }
}